# Melodics Automated Testing System #

This is our in-house automated testing system utilising Qt's debugging and network modules to connect to and test the Melodics client application.

Testing can be done by sending JavaScript expressions to change the state of the QML objects and properties of the client app.

### To-dos ###

* Unit tests for the network and debugging code.
* Configuring the path to the Melodics app to test on.
* Set up the Jenkins project to run it on a Jenkins server.
* Framework and coding/file standards for writing automated tests.
* Identify changes needed in the client application to integrate with the new automated testing system.
* Figure out if it is possible to securely run automated tests with a release build (currently, it seems that the debug port is only available for debug builds).
* QTest module to generate clicks and send key events to the client app.
* Mac testing
* Windows testing