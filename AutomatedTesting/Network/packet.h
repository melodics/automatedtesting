#ifndef PACKET_H
#define PACKET_H

#include <QtCore/qdatastream.h>
#include <QtCore/qbuffer.h>

/*!
  The QPacket class encapsulates an unfragmentable packet of data to be
  transmitted by QPacketProtocol.
  The QPacket class works together with QPacketProtocol to make it simple to
  send arbitrary sized data "packets" across fragmented transports such as TCP
  and UDP.
  QPacket provides a QDataStream interface to an unfragmentable packet.
  Applications should construct a QPacket, propagate it with data and then
  transmit it over a QPacketProtocol instance.

  For example:
  int version = QDataStream::Qt_DefaultCompiledVersion;
  QPacketProtocol protocol(...);
  QPacket myPacket(version);
  myPacket << "Hello world!" << 123;
  protocol.send(myPacket.data());

  As long as both ends of the connection are using the QPacketProtocol class
  and the same data stream version, the data within this packet will be
  delivered unfragmented at the other end, ready for extraction.

  QByteArray greeting;
  int count;
  QPacket myPacket(version, protocol.read());
  myPacket >> greeting >> count;

  Only packets constructed from raw byte arrays may be read from. Empty QPacket
  instances are for transmission only and are considered "write only". Attempting
  to read data from them will result in undefined behavior.

 */

QT_BEGIN_NAMESPACE
class Packet : public QDataStream
{
public:
    /*!
      Constructs an empty write-only packet.
      */
    Packet(int version);

    /*!
      Constructs a read-only packet.
     */
    explicit Packet(int version, const QByteArray &ba);

    /*!
      Returns a reference to the raw packet data.
      */
    const QByteArray &data() const;

    /*!
      Returns a copy of the raw packet data, with extra reserved space removed.
      Mind that this triggers a deep copy. Use it if you anticipate the data to be detached soon anyway.
      */
    QByteArray squeezedData() const;

    /*!
      Clears the packet, discarding any data.
     */
    void clear();
private:
    void init(QIODevice::OpenMode mode);
    QBuffer _buffer;
};
QT_END_NAMESPACE

#endif // PACKET_H
