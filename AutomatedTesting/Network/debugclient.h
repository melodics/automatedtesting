#ifndef DEBUGCLIENT_H
#define DEBUGCLIENT_H

#include <QtCore/qobject.h>

QT_BEGIN_NAMESPACE

class DebugConnection;
class DebugClient : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(DebugClient)
public:
    enum State { NotConnected, Unavailable, Enabled };
    DebugClient(const QString &name, DebugConnection *parent);
    ~DebugClient();

    QString getName() const;
    float serviceVersion() const;
    State state() const;
    void sendMessage(const QByteArray &message);
    DebugConnection* getConnection() const;

signals:
    void stateChanged(State state);
private:
    friend class DebugConnection;
    virtual void messageReceived(const QByteArray &message);
    void addToConnection();

    QString name;
    DebugConnection* connection;
};

QT_END_NAMESPACE
#endif // DEBUGCLIENT_H
