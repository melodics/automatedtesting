#include "packetprotocol.h"

#include <QtCore/QElapsedTimer>
#include <QtCore/QtEndian>

#include "QtCore/qiodevice.h"

QT_BEGIN_NAMESPACE

PacketProtocol::PacketProtocol(QIODevice *dev) :
    _inProgressSize(-1), _waitingForPacket(false), _dev(dev)
{
    Q_ASSERT(4 == sizeof(qint32));
    Q_ASSERT(dev);
    QObject::connect(dev, &QIODevice::readyRead, this, &PacketProtocol::readyToRead);
    QObject::connect(dev, &QIODevice::bytesWritten, this, &PacketProtocol::bytesWritten);
}

void PacketProtocol::send(const QByteArray &data)
{
    static const qint32 maxSize = std::numeric_limits<qint32>::max() - sizeof(qint32);
    if (data.isEmpty()) {
        return; // We don't send empty packets
    }
    if (data.size() > maxSize) {
        emit error();
        return;
    }
    const qint32 sendSize = data.size() + static_cast<qint32>(sizeof(qint32));
    _sendingPackets.append(sendSize);
    qint32 sendSizeLE = qToLittleEndian(sendSize);
    if (!writeToDevice((const char *)&sendSizeLE, sizeof(qint32))
            || !writeToDevice(data.data(), data.size())) {
        emit error();
    }
}

qint64 PacketProtocol::packetsAvailable() const
{
    return _packets.count();
}

QByteArray PacketProtocol::read()
{
    return _packets.isEmpty() ? QByteArray() : _packets.takeFirst();
}

bool PacketProtocol::waitForReadyRead(int msecs)
{
    if (!_packets.isEmpty())
        return true;
    QElapsedTimer stopWatch;
    stopWatch.start();
    _waitingForPacket = true;
    do {
        if (!_dev->waitForReadyRead(msecs)) {
            return false;
        }
        if (!_waitingForPacket) {
            return true;
        }
        msecs = qt_subtract_from_timeout(msecs, stopWatch.elapsed());
    } while (true);
}

int PacketProtocol::qt_subtract_from_timeout(int timeout, int elapsed)
{
    if (timeout == -1) {
        return -1;
    }
    timeout = timeout - elapsed;
    return timeout < 0 ? 0 : timeout;
}

void PacketProtocol::bytesWritten(qint64 bytes)
{
    Q_ASSERT(!_sendingPackets.isEmpty());
    while (bytes) {
        if (_sendingPackets.at(0) > bytes) {
            _sendingPackets[0] -= bytes;
            bytes = 0;
        } else {
            bytes -= _sendingPackets.at(0);
            _sendingPackets.removeFirst();
        }
    }
}
void PacketProtocol::readyToRead()
{
    while (true) {
        // Need to get trailing data
        if (-1 == _inProgressSize) {
            // We need a size header of sizeof(qint32)
            if (static_cast<qint64>(sizeof(qint32)) > _dev->bytesAvailable()) {
                return;
            }
            // Read size header
            qint32 _inProgressSizeLE;
            if (!readFromDevice((char *)&_inProgressSizeLE, sizeof(qint32))) {
                emit error();
                return;
            }
            _inProgressSize = qFromLittleEndian(_inProgressSizeLE);
            // Check sizing constraints
            if (_inProgressSize < qint32(sizeof(qint32))) {
                disconnect(_dev, &QIODevice::readyRead, this, &PacketProtocol::readyToRead);
                disconnect(_dev, &QIODevice::bytesWritten, this, &PacketProtocol::bytesWritten);
                _dev = nullptr;
                emit error();
                return;
            }
            _inProgressSize -= sizeof(qint32);
        } else {
            const int bytesToRead = static_cast<int>(
                        qMin(_dev->bytesAvailable(),
                             static_cast<qint64>(_inProgressSize - _inProgress.size())));
            QByteArray toRead(bytesToRead, Qt::Uninitialized);
            if (!readFromDevice(toRead.data(), toRead.length())) {
                emit error();
                return;
            }
            _inProgress.append(toRead);
            if (_inProgressSize == _inProgress.size()) {
                // Packet has arrived!
                _packets.append(_inProgress);
                _inProgressSize = -1;
                _inProgress.clear();
                _waitingForPacket = false;
                emit readyRead();
            } else {
                return;
            }
        }
    }
}

bool PacketProtocol::writeToDevice(const char *bytes, qint64 size)
{
    qint64 totalWritten = 0;
    while (totalWritten < size) {
        const qint64 chunkSize = _dev->write(bytes + totalWritten, size - totalWritten);
        if (chunkSize < 0) {
            return false;
        }
        totalWritten += chunkSize;
    }
    return totalWritten == size;
}

bool PacketProtocol::readFromDevice(char *buffer, qint64 size)
{
    qint64 totalRead = 0;
    while (totalRead < size) {
        const qint64 chunkSize = _dev->read(buffer + totalRead, size - totalRead);
        if (chunkSize < 0) {
            return false;
        }
        totalRead += chunkSize;
    }
    return totalRead == size;
}

QT_END_NAMESPACE
