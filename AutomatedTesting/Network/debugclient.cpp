#include "debugclient.h"
#include "debugconnection.h"
#include <QtCore/qdebug.h>
#include <QtCore/qpointer.h>

QT_BEGIN_NAMESPACE
DebugClient::DebugClient(const QString &name, DebugConnection *connection) :
    name(name), connection(connection)
{
    addToConnection();
}

DebugClient::~DebugClient()
{
    if (connection && !connection->removeClient(name))
        qWarning() << "DebugClient: Plugin not registered" << name;
}

void DebugClient::addToConnection()
{
    if (connection && !connection->addClient(name, this)) {
        qWarning() << "DebugClient: Conflicting plugin name" << name;
        connection = nullptr;
    }
}

QString DebugClient::getName() const
{
    return name;
}

float DebugClient::serviceVersion() const
{
    return connection->serviceVersion(name);
}

DebugClient::State DebugClient::state() const
{
    if (!connection || !connection->isConnected())
        return NotConnected;
    if (connection->serviceVersion(name) != -1)
        return Enabled;
    return Unavailable;
}

void DebugClient::sendMessage(const QByteArray &message)
{
    connection->sendMessage(name, message);
}

DebugConnection *DebugClient::getConnection() const
{
    return connection;
}

void DebugClient::messageReceived(const QByteArray &message)
{
    Q_UNUSED(message);
}
QT_END_NAMESPACE
