#ifndef DEBUGCLIENTIMPL_H
#define DEBUGCLIENTIMPL_H


#include "debugclient.h"
#include <QtCore/qjsonvalue.h>

QT_BEGIN_NAMESPACE
class DebugClientImpl : public DebugClient
{
    Q_OBJECT
public:
    enum StepAction
    {
        Continue,
        In,
        Out,
        Next
    };

    enum Exception
    {
        All,
        Uncaught
    };

    struct Response
    {
        QString command;
        QJsonValue body;
    };

    DebugClientImpl(const QString &name, DebugConnection *connection);

    void connect();
    void disconnect();
    void interrupt();
    void continueDebugging(StepAction stepAction);
    void evaluate(const QString &expr, int frame = -1, int context = -1);
    void lookup(const QList<int> &handles, bool includeSource = false);
    void backtrace(int fromFrame = -1, int toFrame = -1, bool bottom = false);
    void frame(int number = -1);
    void scope(int number = -1, int frameNumber = -1);
    void scripts(int types = 4, const QList<int> &ids = QList<int>(), bool includeSource = false);
    void setBreakpoint(const QString &target, int line = -1, int column = -1, bool enabled = true,
                       const QString &condition = QString(), int ignoreCount = -1);
    void clearBreakpoint(int breakpoint);
    void changeBreakpoint(int breakpoint, bool enabled);
    void setExceptionBreak(Exception type, bool enabled = false);
    void version();
    Response getResponse() const;
protected:
    void messageReceived(const QByteArray &data) override;
signals:
    void connected();
    void interrupted();
    void result();
    void failure();
    void stopped();
private:
    void onStateChanged(DebugClient::State state);
    void sendMessage(const QByteArray &command, const QJsonObject &args);
    void flushSendBuffer();
    QByteArray packMessage(const QByteArray &type, const QJsonObject &object);

    int seq = 0;
    QList<QByteArray> sendBuffer;
    QByteArray response;
};
QT_END_NAMESPACE

#endif // DEBUGCLIENTIMPL_H
