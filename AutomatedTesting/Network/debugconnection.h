#ifndef DEBUGCONNECTION_H
#define DEBUGCONNECTION_H

#include <QtCore/qobject.h>
#include <QtCore/qeventloop.h>
#include <QtCore/qtimer.h>
#include <QtCore/qdatastream.h>
#include <QtNetwork/qabstractsocket.h>
#include <QtNetwork/qlocalserver.h>
#include <QtNetwork/qlocalsocket.h>
#include <QtNetwork/qtcpsocket.h>

#include "packetprotocol.h"
#include "packet.h"

QT_BEGIN_NAMESPACE

class LocalSocketSignalTranslator : public QObject
{
    Q_OBJECT
public:
    LocalSocketSignalTranslator(QLocalSocket *parent) : QObject(parent)
    {
        connect(parent, &QLocalSocket::stateChanged,
                this, &LocalSocketSignalTranslator::onStateChanged);
        connect(parent, static_cast<void(QLocalSocket::*)(QLocalSocket::LocalSocketError)>(
                    &QLocalSocket::error), this, &LocalSocketSignalTranslator::onError);
    }
    void onError(QLocalSocket::LocalSocketError error)
    {
        emit socketError(static_cast<QAbstractSocket::SocketError>(error));
    }
    void onStateChanged(QLocalSocket::LocalSocketState state)
    {
        emit socketStateChanged(static_cast<QAbstractSocket::SocketState>(state));
    }
signals:
    void socketError(QAbstractSocket::SocketError error);
    void socketStateChanged(QAbstractSocket::SocketState state);
};

class DebugClient;
class DebugConnection : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(DebugConnection)
public:
    DebugConnection();
    ~DebugConnection();
    void connectToHost(const QString &hostName, quint16 port);
    void startLocalServer(const QString &fileName);
    int getCurrentDataStreamVersion() const;
    void setMaximumDataStreamVersion(int maximumVersion);
    bool isConnected() const;
    bool isConnecting() const;
    void close();
    bool waitForConnected(int msecs = 30000);
    DebugClient *client(const QString &name) const;
    bool addClient(const QString &name, DebugClient *client);
    bool removeClient(const QString &name);
    float serviceVersion(const QString &serviceName) const;
    bool sendMessage(const QString &name, const QByteArray &message);
signals:
    void connected();
    void disconnected();
    void socketError(QAbstractSocket::SocketError socketError);
    void socketStateChanged(QAbstractSocket::SocketState socketState);
private:
    void newConnection();
    void socketConnected();
    void socketDisconnected();
    void protocolReadyRead();
    void handshakeTimeout();

    PacketProtocol* _protocol = nullptr;
    QIODevice* _device = nullptr;

    QLocalServer* _server = nullptr;
    QEventLoop _handshakeEventLoop;
    QTimer _handshakeTimer;

    bool _gotHello = false;
    int _currentDataStreamVersion = QDataStream::Qt_4_7;
    int _maximumDataStreamVersion = QDataStream::Qt_DefaultCompiledVersion;

    QHash <QString, float> _serverPlugins;
    QHash<QString, DebugClient *> _plugins;
    QStringList _removedPlugins;

    void advertisePlugins();
    void createProtocol();
    void flush();
};
QT_END_NAMESPACE

#endif // DEBUGCONNECTION_H
