#include "debugconnection.h"

#include "debugclient.h"

QT_BEGIN_NAMESPACE
static const int protocolVersion = 1;
static const QString serverId = QLatin1String("DebugServer");
static const QString clientId = QLatin1String("DebugClient");

void DebugConnection::advertisePlugins()
{
    if (!isConnected()) {
        return;
    }

    Packet pack(_currentDataStreamVersion);
    pack << serverId << 1 << _plugins.keys();
    _protocol->send(pack.data());
    flush();
}

void DebugConnection::socketConnected()
{
    Packet pack(_currentDataStreamVersion);
    pack << serverId << 0 << protocolVersion << _plugins.keys() << _maximumDataStreamVersion
         << true; // We accept multiple messages per packet
    _protocol->send(pack.data());
    flush();
}

void DebugConnection::socketDisconnected()
{
    _gotHello = false;
    emit disconnected();
}

void DebugConnection::protocolReadyRead()
{
    if (!_gotHello) {
        Packet pack(_currentDataStreamVersion, _protocol->read());
        QString name;
        pack >> name;
        bool validHello = false;
        if (name == clientId) {
            int op = -1;
            pack >> op;
            if (op == 0) {
                int version = -1;
                pack >> version;
                if (version == protocolVersion) {
                    QStringList pluginNames;
                    QList<float> pluginVersions;
                    pack >> pluginNames;
                    if (!pack.atEnd())
                        pack >> pluginVersions;
                    const int pluginNamesSize = pluginNames.size();
                    const int pluginVersionsSize = pluginVersions.size();
                    for (int i = 0; i < pluginNamesSize; ++i) {
                        float pluginVersion = 1.0;
                        if (i < pluginVersionsSize)
                            pluginVersion = pluginVersions.at(i);
                        _serverPlugins.insert(pluginNames.at(i), pluginVersion);
                    }
                    pack >> _currentDataStreamVersion;
                    validHello = true;
                }
            }
        }

        if (!validHello) {
            qWarning("DebugConnection: Invalid hello message");
            close();
            return;
        }

        _gotHello = true;
        emit connected();
        QHash<QString, DebugClient *>::Iterator iter = _plugins.begin();
        for (; iter != _plugins.end(); ++iter) {
            DebugClient::State newState = DebugClient::Unavailable;
            if (_serverPlugins.contains(iter.key()))
                newState = DebugClient::Enabled;
            iter.value()->stateChanged(newState);
        }
        _handshakeTimer.stop();
        _handshakeEventLoop.quit();
    }

    while (_protocol->packetsAvailable()) {
        Packet pack(_currentDataStreamVersion, _protocol->read());
        QString name;
        pack >> name;
        if (name == clientId) {
            int op = -1;
            pack >> op;
            if (op == 1) {
                // Service Discovery
                QHash<QString, float> oldServerPlugins = _serverPlugins;
                _serverPlugins.clear();
                QStringList pluginNames;
                QList<float> pluginVersions;
                pack >> pluginNames;
                if (!pack.atEnd())
                    pack >> pluginVersions;
                const int pluginNamesSize = pluginNames.size();
                const int pluginVersionsSize = pluginVersions.size();
                for (int i = 0; i < pluginNamesSize; ++i) {
                    float pluginVersion = 1.0;
                    if (i < pluginVersionsSize)
                        pluginVersion = pluginVersions.at(i);
                    _serverPlugins.insert(pluginNames.at(i), pluginVersion);
                }
                QHash<QString, DebugClient *>::Iterator iter = _plugins.begin();
                for (; iter != _plugins.end(); ++iter) {
                    const QString pluginName = iter.key();
                    DebugClient::State newSate = DebugClient::Unavailable;
                    if (_serverPlugins.contains(pluginName))
                        newSate = DebugClient::Enabled;
                    if (oldServerPlugins.contains(pluginName)
                            != _serverPlugins.contains(pluginName)) {
                        iter.value()->stateChanged(newSate);
                    }
                }
            } else {
                qWarning() << "QQmlDebugConnection: Unknown control message id" << op;
            }
        } else {
            QHash<QString, DebugClient *>::Iterator iter = _plugins.find(name);
            if (iter == _plugins.end()) {
                // We can get more messages for _plugins we have removed because it takes time to
                // send the advertisement message but the removal is instant locally.
                if (!_removedPlugins.contains(name))
                    qWarning() << "QQmlDebugConnection: Message received for missing plugin"
                               << name;
            } else {
                DebugClient *client = *iter;
                QByteArray message;
                while (!pack.atEnd()) {
                    pack >> message;
                    client->messageReceived(message);
                }
            }
        }
    }
}

void DebugConnection::handshakeTimeout()
{
    if (!_gotHello) {
        qWarning() << "QQmlDebugConnection: Did not get handshake answer in time";
        _handshakeEventLoop.quit();
    }
}

DebugConnection::DebugConnection()
{
    _handshakeTimer.setSingleShot(true);
    _handshakeTimer.setInterval(3000);

    connect(&_handshakeTimer, &QTimer::timeout, this, &DebugConnection::handshakeTimeout);
}

DebugConnection::~DebugConnection()
{
    QHash<QString, DebugClient*>::iterator iter = _plugins.begin();
    for (; iter != _plugins.end(); ++iter)
        emit iter.value()->stateChanged(DebugClient::NotConnected);
}

int DebugConnection::getCurrentDataStreamVersion() const
{
    return _currentDataStreamVersion;
}

void DebugConnection::setMaximumDataStreamVersion(int maximumVersion)
{
    _maximumDataStreamVersion = maximumVersion;
}

bool DebugConnection::isConnected() const
{
    return _gotHello;
}

bool DebugConnection::isConnecting() const
{
    return !_gotHello && _device;
}

void DebugConnection::close()
{
    if (_gotHello) {
        _gotHello = false;
        _device->close();
        QHash<QString, DebugClient*>::iterator iter = _plugins.begin();
        for (; iter != _plugins.end(); ++iter)
            emit iter.value()->stateChanged(DebugClient::NotConnected);
    }
    if (_device) {
        _device->deleteLater();
        _device = nullptr;
    }
}

bool DebugConnection::waitForConnected(int msecs)
{
    QAbstractSocket *socket = qobject_cast<QAbstractSocket*>(_device);
    if (!socket) {
        if (!_server || (!_server->hasPendingConnections() &&
                           !_server->waitForNewConnection(msecs)))
            return false;
    } else if (!socket->waitForConnected(msecs)) {
        return false;
    }
    // wait for handshake
    _handshakeTimer.start();
    _handshakeEventLoop.exec();
    return _gotHello;
}

DebugClient *DebugConnection::client(const QString &name) const
{
    return _plugins.value(name, 0);
}

bool DebugConnection::addClient(const QString &name, DebugClient *client)
{
    if (_plugins.contains(name))
        return false;
    _removedPlugins.removeAll(name);
    _plugins.insert(name, client);
    advertisePlugins();
    return true;
}

bool DebugConnection::removeClient(const QString &name)
{
    if (!_plugins.contains(name))
        return false;
    _plugins.remove(name);
    _removedPlugins.append(name);
    advertisePlugins();
    return true;
}

float DebugConnection::serviceVersion(const QString &serviceName) const
{
    return _serverPlugins.value(serviceName, -1);
}

bool DebugConnection::sendMessage(const QString &name, const QByteArray &message)
{
    if (!isConnected() || !_serverPlugins.contains(name))
        return false;
    Packet pack(_currentDataStreamVersion);
    pack << name << message;
    _protocol->send(pack.data());
    flush();
    return true;
}

void DebugConnection::flush()
{
    if (QAbstractSocket *socket = qobject_cast<QAbstractSocket *>(_device))
        socket->flush();
    else if (QLocalSocket *socket = qobject_cast<QLocalSocket *>(_device))
        socket->flush();
}

void DebugConnection::connectToHost(const QString &hostName, quint16 port)
{
    if (_gotHello)
        close();
    QTcpSocket *socket = new QTcpSocket(this);
    _device = socket;
    createProtocol();
    connect(socket, &QAbstractSocket::disconnected, this, &DebugConnection::socketDisconnected);
    connect(socket, &QAbstractSocket::connected, this, &DebugConnection::socketConnected);
    connect(socket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(
                &QAbstractSocket::error), this, &DebugConnection::socketError);
    connect(socket, &QAbstractSocket::stateChanged, this, &DebugConnection::socketStateChanged);
    socket->connectToHost(hostName, port);
}

void DebugConnection::startLocalServer(const QString &fileName)
{
    if (_gotHello)
        close();
    if (_server)
        _server->deleteLater();
    _server = new QLocalServer(this);
    // QueuedConnection so that waitForNewConnection() returns true.
    connect(_server, &QLocalServer::newConnection,
            this, &DebugConnection::newConnection, Qt::QueuedConnection);
    _server->listen(fileName);
}

void DebugConnection::newConnection()
{
    delete _device;
    QLocalSocket *socket = _server->nextPendingConnection();
    _server->close();
    _device = socket;
    createProtocol();
    connect(socket, &QLocalSocket::disconnected, this, &DebugConnection::socketDisconnected);
    LocalSocketSignalTranslator *translator = new LocalSocketSignalTranslator(socket);
    connect(translator, &LocalSocketSignalTranslator::socketError,
            this, &DebugConnection::socketError);
    connect(translator, &LocalSocketSignalTranslator::socketStateChanged,
            this, &DebugConnection::socketStateChanged);
    socketConnected();
}

void DebugConnection::createProtocol()
{
    delete _protocol;
    _protocol = new PacketProtocol(_device);
    QObject::connect(_protocol, &PacketProtocol::readyRead,
                     this, &DebugConnection::protocolReadyRead);
}

QT_END_NAMESPACE
