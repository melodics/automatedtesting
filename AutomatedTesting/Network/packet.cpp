#include "packet.h"

QT_BEGIN_NAMESPACE

Packet::Packet(int version)
{
    _buffer.open(QIODevice::WriteOnly);
    setDevice(&_buffer);
    setVersion(version);
}

Packet::Packet(int version, const QByteArray &data)
{
    _buffer.setData(data);
    _buffer.open(QIODevice::ReadOnly);
    setDevice(&_buffer);
    setVersion(version);
}

const QByteArray&
Packet::data() const
{
    return _buffer.data();
}

QByteArray
Packet::squeezedData() const
{
    QByteArray ret = _buffer.data();
    ret.squeeze();
    return ret;
}

void
Packet::clear()
{
    _buffer.reset();
    QByteArray &buffer = _buffer.buffer();
    // Keep the old size to prevent unnecessary allocations
    buffer.reserve(buffer.capacity());
    buffer.truncate(0);
}

QT_END_NAMESPACE
