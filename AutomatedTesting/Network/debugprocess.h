#ifndef DEBUGSERVER_H
#define DEBUGSERVER_H

#include <QtCore/qprocess.h>
#include <QtCore/qtimer.h>
#include <QtCore/qeventloop.h>
#include <QtCore/qmutex.h>

class DebugProcess : public QObject
{
    Q_OBJECT
public:
    DebugProcess(const QString &executable);
    ~DebugProcess();

    QString stateString() const;
    void addEnvironment(const QString &environment);
    void start(const QStringList &arguments);
    bool waitForSessionStart();
    int debugPort() const;
    bool waitForFinished();

    QProcess::ProcessState state() const;
    QProcess::ExitStatus exitStatus() const;
    QString output() const;
    void stop();
    void setMaximumBindErrors(int numErrors);
signals:
    void readyReadStandardOutput();
    void finished();
private slots:
    void timeout();
    void processAppOutput();
    void processError(QProcess::ProcessError error);
private:
    enum SessionState {
        SessionUnknown,
        SessionStarted,
        SessionFailed
    };

    QString      _executable;
    QProcess     _process;
    QString      _outputBuffer;
    QString      _output;
    QTimer       _timer;
    QEventLoop   _eventLoop;
    QMutex       _mutex;
    SessionState _state;
    QStringList  _environment;

    int _port;
    int _maximumBindErrors;
    int _receivedBindErrors;
};

#endif // DEBUGSERVER_H
