#include "debugprocess.h"

#include <QtCore/qdebug.h>
#include <QtCore/qfileinfo.h>
#include <QtCore/qdir.h>

DebugProcess::DebugProcess(const QString &executable)
    : _executable(executable)
    , _state(SessionUnknown)
    , _port(0)
    , _maximumBindErrors(0)
    , _receivedBindErrors(0)
{
    _process.setProcessChannelMode(QProcess::MergedChannels);
    _timer.setInterval(15000);
    connect(&_process, &QProcess::readyReadStandardOutput,
            this, &DebugProcess::processAppOutput);
    connect(&_process, &QProcess::errorOccurred,
            this, &DebugProcess::processError);
    connect(&_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this, [this]() {
        _timer.stop();
        _eventLoop.quit();
        emit finished();
    });
    connect(&_timer, &QTimer::timeout,
            this, &DebugProcess::timeout);
}

DebugProcess::~DebugProcess()
{
    stop();
}

QString DebugProcess::stateString() const
{
    QString stateStr;
    switch (_process.state()) {
    case QProcess::NotRunning: {
        stateStr = "not running";
        if (_process.exitStatus() == QProcess::CrashExit) {
            stateStr += " (crashed!)";
        } else {
            stateStr += ", return value " + QString::number(_process.exitCode());
        }
        break;
    }
    case QProcess::Starting:
        stateStr = "starting";
        break;
    case QProcess::Running:
        stateStr = "running";
        break;
    }
    return stateStr;
}

void DebugProcess::start(const QStringList &arguments)
{
#ifdef Q_OS_MAC
    // make sure _executable points to the actual binary even if it's inside an app bundle
    QFileInfo binFile(_executable);
    if (!binFile.isExecutable()) {
        QDir bundleDir(_executable + ".app");
        if (bundleDir.exists()) {
            _executable = bundleDir.absoluteFilePath("Contents/MacOS/" + binFile.baseName());
            qDebug() << Q_FUNC_INFO << "found bundled binary" << _executable;
        }
    }
#endif
    _mutex.lock();
    _port = 0;
    _process.setEnvironment(QProcess::systemEnvironment() + _environment);
    _process.start(_executable, arguments);
    if (!_process.waitForStarted()) {
        qWarning() << "QML Debug Client: Could not launch app " << _executable
                   << ": " << _process.errorString();
        _eventLoop.quit();
    }
    _mutex.unlock();

    qDebug() << "finish start";
}

void DebugProcess::stop()
{
    if (_process.state() != QProcess::NotRunning) {
        disconnect(&_process, &QProcess::errorOccurred, this, &DebugProcess::processError);
        _process.kill();
        _process.waitForFinished(5000);
    }
}

void DebugProcess::setMaximumBindErrors(int ignore)
{
    _maximumBindErrors = ignore;
}

void DebugProcess::timeout()
{
    qWarning() << "Timeout while waiting for QML debugging messages "
                  "in application output. Process is in state" << _process.state()
               << ", Output:" << _output << ".";
}

bool DebugProcess::waitForSessionStart()
{
    if (_process.state() != QProcess::Running) {
        qWarning() << "Could not start up " << _executable;
        return false;
    } else if (_state == SessionFailed) {
        return false;
    } else if (_state == SessionStarted) {
        return true;
    }

    _timer.start();
    _eventLoop.exec();

    return _state == SessionStarted;
}

int DebugProcess::debugPort() const
{
    return _port;
}

bool DebugProcess::waitForFinished()
{
    return _process.waitForFinished();
}

QProcess::ProcessState DebugProcess::state() const
{
    return _process.state();
}

QProcess::ExitStatus DebugProcess::exitStatus() const
{
    return _process.exitStatus();
}

void DebugProcess::addEnvironment(const QString &environment)
{
    _environment.append(environment);
}

QString DebugProcess::output() const
{
    return _output;
}

void DebugProcess::processAppOutput()
{
    _mutex.lock();
    bool outputFromAppItself = false;
    QString newOutput = _process.readAll();
    _output.append(newOutput);
    _outputBuffer.append(newOutput);
    while (true) {
        const int nlIndex = _outputBuffer.indexOf(QLatin1Char('\n'));
        if (nlIndex < 0) { // no further complete lines
            break;
        }
        const QString line = _outputBuffer.left(nlIndex);
        _outputBuffer = _outputBuffer.right(_outputBuffer.size() - nlIndex - 1);
        if (line.contains("QML Debugger:")) {
            const QRegExp portRx("Waiting for connection on port (\\d+)");
            if (portRx.indexIn(line) != -1) {
                _port = portRx.cap(1).toInt();
                _timer.stop();
                _state = SessionStarted;
                _eventLoop.quit();
                continue;
            }
            if (line.contains("Unable to listen")) {
                if (++_receivedBindErrors >= _maximumBindErrors) {
                    if (_maximumBindErrors == 0)
                        qWarning() << "App was unable to bind to port!";
                    _timer.stop();
                    _state = SessionFailed;
                    _eventLoop.quit();
                 }
                 continue;
            }
        }
        // set to true if there is output not coming from the debugger or we don't understand it
        outputFromAppItself = true;
    }

    _mutex.unlock();
    if (outputFromAppItself) {
        emit readyReadStandardOutput();
    }
}

void DebugProcess::processError(QProcess::ProcessError error)
{
    qDebug() << "An error occurred while waiting for debug process to become available:";
    switch (error) {
    case QProcess::FailedToStart:
        qDebug() << "Process failed to start.";
        break;
    case QProcess::Crashed:
        qDebug() << "Process crashed.";
        break;
    case QProcess::Timedout:
        qDebug() << "Process timed out.";
        break;
    case QProcess::WriteError:
        qDebug() << "Error while writing to process.";
        break;
    case QProcess::ReadError:
        qDebug() << "Error while reading from process.";
        break;
    case QProcess::UnknownError:
        qDebug() << "Unknown process error.";
        break;
    }
}
