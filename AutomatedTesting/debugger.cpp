#include "debugger.h"

#include "Network/debugprocess.h"
#include "Network/debugconnection.h"

#include <QtCore/qeventloop.h>
#include <QtCore/qtimer.h>
#include <QtCore/QCoreApplication>
#include <QDir>

bool Debugger::waitForSignal(QObject *receiver, const char *member, int timeout) {
    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    QObject::connect(receiver, member, &loop, SLOT(quit()));
    timer.start(timeout);
    loop.exec();
    if (!timer.isActive()) {
        qWarning("waitForSignal %s timed out after %d ms", member, timeout);
    }
    return timer.isActive();
}

QList<DebugClient *> Debugger::createOtherClients(DebugConnection *connection)
{
    QList<DebugClient *> ret;
    static const auto debuggerServices
            = QStringList({"V8Debugger", "QmlDebugger", "DebugMessages"});
    static const auto inspectorServices
            = QStringList({"QmlInspector"});
    static const auto profilerServices
            = QStringList({"CanvasFrameRate", "EngineControl", "DebugMessages"});
    for (const QString &service : debuggerServices) {
        if (!connection->client(service))
            ret << new DebugClient(service, connection);
    }
    for (const QString &service : inspectorServices) {
        if (!connection->client(service))
            ret << new DebugClient(service, connection);
    }
    for (const QString &service : profilerServices) {
        if (!connection->client(service))
            ret << new DebugClient(service, connection);
    }
    return ret;
}
QString Debugger::clientStateString(const DebugClient *client)
{
    if (!client)
        return QLatin1String("null");
    switch (client->state()) {
    case DebugClient::NotConnected: return QLatin1String("Not connected");
    case DebugClient::Unavailable: return QLatin1String("Unavailable");
    case DebugClient::Enabled: return QLatin1String("Enabled");
    default: return QLatin1String("Invalid");
    }
}

QString Debugger::connectionStateString(const DebugConnection *connection)
{
    if (!connection)
        return QLatin1String("null");
    return connection->isConnected() ? QLatin1String("connected") : QLatin1String("not connected");
}

Debugger::ConnectResult Debugger::connect(
        const QString &executable, const QString &services, const QString &extraArgs,
        bool block)
{
    qDebug() << "connecting";
    QStringList arguments;
    arguments << QString::fromLatin1("-qmljsdebugger=port:13773,13783%3%4")
                 .arg(block ? QStringLiteral(",block") : QString())
                 .arg(services.isEmpty() ? services : (QStringLiteral(",services:") + services))
              << extraArgs;

    m_process = createProcess(executable);
    if (!m_process) {
        qDebug() << "create process failed";
        return ProcessFailed;
    }

    qDebug() << "process created";

    m_process->start(QStringList() << arguments);
    if (!m_process->waitForSessionStart()) {
        qDebug() << "process start failed";
        return SessionFailed;
    }

    qDebug() << "process started";

    m_connection = createConnection();
    if (!m_connection) {
        qDebug() << "connection failed";
        return ConnectionFailed;
    }

    qDebug() << "connection created";

    m_clients = createClients();
    if (m_clients.contains(nullptr)) {
        qDebug() << "create clients failed";
        return ClientsFailed;
    }

    qDebug() << "create clients";

    ClientStateHandler stateHandler(m_clients, createOtherClients(m_connection), services.isEmpty()
                                    ? DebugClient::Enabled : DebugClient::Unavailable);


    const int port = m_process->debugPort();
    m_connection->connectToHost(QLatin1String("127.0.0.1"), port);
    QEventLoop loop;
    QTimer timer;
    QObject::connect(&stateHandler, &ClientStateHandler::allOk, &loop, &QEventLoop::quit);
    QObject::connect(m_connection, &DebugConnection::disconnected, &loop, &QEventLoop::quit);
    QObject::connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    timer.start(5000);
    loop.exec();
    qDebug() << "timer end";
    if (!stateHandler.allEnabled()) {
        return EnableFailed;
    }

    if (!stateHandler.othersAsExpected()) {
        return RestrictFailed;
    }

    return ConnectSuccess;
}

QPointer<DebugClientImpl> Debugger::getClient()
{
    return _client;
}

QList<DebugClient *> Debugger::createClients()
{
    _client = new DebugClientImpl("V8Debugger", m_connection);
    return QList<DebugClient *>({_client});
}

DebugProcess *Debugger::createProcess(const QString &executable)
{
    return new DebugProcess(executable);
}

DebugConnection *Debugger::createConnection()
{
    return new DebugConnection();
}

void Debugger::cleanup()
{
//    if (QTest::currentTestFailed()) {
//        const QString null = QStringLiteral("null");
//        qDebug() << "Process State:" << (m_process ? m_process->stateString() : null);
//        qDebug() << "Application Output:" << (m_process ? m_process->output() : null);
//        qDebug() << "Connection State:" << Debugger::connectionStateString(m_connection);
//        for (DebugClient *client : m_clients) {
//            if (client)
//                qDebug() << client->getName() << "State:" << Debugger::clientStateString(client);
//            else
//                qDebug() << "Failed Client:" << null;
//        }
//    }
    qDeleteAll(m_clients);
    m_clients.clear();
    delete m_connection;
    m_connection = nullptr;
    if (m_process) {
        m_process->stop();
        delete m_process;
        m_process = nullptr;
    }
}

ClientStateHandler::ClientStateHandler(const QList<DebugClient *> &clients,
                                       const QList<DebugClient *> &others,
                                       DebugClient::State expectedOthers) :
    m_clients(clients), m_others(others), m_expectedOthers(expectedOthers)
{
    for (DebugClient *client : m_clients) {
        QObject::connect(client, &DebugClient::stateChanged,
                         this, &ClientStateHandler::checkStates);
    }
    for (DebugClient *client : m_others) {
        QObject::connect(client, &DebugClient::stateChanged,
                         this, &ClientStateHandler::checkStates);
    }
}

ClientStateHandler::~ClientStateHandler()
{
    qDeleteAll(m_others);
}

void ClientStateHandler::checkStates()
{
    for (DebugClient *client : m_clients) {
        if (client->state() != DebugClient::Enabled)
            return;
    }
    m_allEnabled = true;
    for (DebugClient *other : m_others) {
        if (other->state() != m_expectedOthers)
            return;
    }
    m_othersAsExpected = true;
    emit allOk();
}

QString getServerPath()
{
    // todo: need to figure out how to automatically find the app path for both OSes and for the Jenkins environment
    const QString selfPath = "Melodics";
    return "/Users/hunwoo/Documents/build-project-Desktop_Qt_5_12_7_clang_64bit-Debug/app/" + selfPath;
}
