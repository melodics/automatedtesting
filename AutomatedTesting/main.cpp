#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>

#include "debugger.h"
#include "Network/debugclientimpl.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    Debugger debugger;
    debugger.connect(getServerPath(), QStringLiteral("V8Debugger"), "", true);

    QPointer<DebugClientImpl> client = debugger.getClient();

    client->connect();

    client->scripts();

    qDebug() << client->getResponse().command;

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
