#ifndef DEBUGGER_H
#define DEBUGGER_H

#include <QPointer>

#include "Network/debugclientimpl.h"
#include "Network/debugclient.h"

class DebugClientImpl;
class DebugProcess;
class Debugger : QObject
{
    Q_OBJECT
public:
    static bool waitForSignal(QObject *receiver, const char *member, int timeout = 5000);
    static QList<DebugClient *> createOtherClients(DebugConnection *connection);
    static QString clientStateString(const DebugClient *client);
    static QString connectionStateString(const DebugConnection *connection);

    enum ConnectResult {
        ConnectSuccess,
        ProcessFailed,
        SessionFailed,
        ConnectionFailed,
        ClientsFailed,
        EnableFailed,
        RestrictFailed
    };

    ConnectResult connect(const QString &executable, const QString &services,
                          const QString &extraArgs, bool block);

    QPointer<DebugClientImpl> getClient();

private:
    DebugProcess *createProcess(const QString &executable);
    DebugConnection *createConnection();
    QList<DebugClient *> createClients();

    QPointer<DebugClientImpl> _client;

    DebugProcess *m_process = nullptr;
    DebugConnection *m_connection = nullptr;
    QList<DebugClient *> m_clients;
private slots:
    virtual void cleanup();
};

class ClientStateHandler : public QObject
{
    Q_OBJECT
public:
    ClientStateHandler(const QList<DebugClient *> &clients,
                       const QList<DebugClient *> &others,
                       DebugClient::State expectedOthers);
    ~ClientStateHandler();
    bool allEnabled() const { return m_allEnabled; }
    bool othersAsExpected() const { return m_othersAsExpected; }
signals:
    void allOk();
private:
    void checkStates();
    const QList<DebugClient *> m_clients;
    const QList<DebugClient *> m_others;
    const DebugClient::State m_expectedOthers;
    bool m_allEnabled = false;
    bool m_othersAsExpected = false;
};

QString getServerPath();

#endif // DEBUGGER_H
